const express = require('express')
const app = express()
const port = 3000
const https = require('https');
const fs = require('fs');
const cors = require('cors');
const bodyParser=require('body-parser')

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.get('/', (req, res) => res.send('Hello World!'))
app.get('/', (req, res) => 
{
  console.log('get');
  res.send('Hellow world!');
});
// app.post('/input', (req, res) => 
// {
//     // console.log(req);
//     console.log(req.body);
//     console.log(req.body.name);
//     if (req.body.name)
//     {
//         res.json({name: req.body.name, status: 'success'});
//     }
//     else
//     res.json({status: 'Failed'});
// })

app.post('/input', (req, res) => {
    
    console.log('Got body:', req.body);
    console.log(req.body.id);
    if (req.body.id)
    res.send('success');
    else
    res.send('fail');
});


https.createServer({
    key: fs.readFileSync('C:/Windows/System32/client-1.local.key'),
    cert: fs.readFileSync('C:/Windows/System32/client-1.local.crt')
}, app).listen(3000, () => {
  console.log('Listening...')
})