/* eslint-env node */
/*
 * @license
 * Your First PWA Codelab (https://g.co/codelabs/pwa)
 * Copyright 2019 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
'use strict';
const https = require('https');
const cors = require('cors')
const fs = require('fs');
const express = require('express');

const path = require('path');
//const redirectToHTTPS = require('express-http-to-https').redirectToHTTPS;







/**
 * Starts the Express server.
 *
 * @return {ExpressServer} instance of the Express server.
 */
function startServer() {
  const app = express();


 

  
  app.use(cors());


  // Handle requests for static files
  // add all static file
  app.use(express.static('public'));

  app.get('/', function(req, res)
  {
    res.sendFile('public/index.html', {root: __dirname});
  })


  // Start the server
  return https.createServer({
    key: fs.readFileSync('C:/Windows/System32/client-1.local.key'),
    cert: fs.readFileSync('C:/Windows/System32/client-1.local.crt')
}, app).listen(8000, () => {
  console.log('Listening on port 8000')
})
}

startServer();
