/*
 * @license
 * Your First PWA Codelab (https://g.co/codelabs/pwa)
 * Copyright 2019 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
'use strict';

// document.write("<script language=javascript src='/indexeddb.js'");

var index = 0;
const dbName = 'clientDb';
const tableName = 'info'

const corsHeaders = new Headers({
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json'
});


function fillSp1() {
  // return fetch(`https://www.google.com`,
  return fetch(`https://localhost:3000`,
  {
    method: 'GET',
    headers: corsHeaders,
    mode: 'cors'
  }
  )
  .then((res) =>
  {
   log(res.body);
    res.text().then(value=>
      {
        document.getElementById('sp1').textContent = value;
      })
     
  })
  .catch(err =>
    {
     log('failed to get info');
    });
}
function getImageFile(callback)
{
  
  const file = document.getElementById('insuranceImage').files[0];
  const reader = new FileReader();

  reader.onload = function()
  {
    callback(reader.result);
  };

  if (file) {
    reader.readAsDataURL(file);
  }
    
}
 
function sendMessage()
{
  const id = 'id';
  const idxs = ['details'];

  getImageFile(function(imageFile)
  {
    const now = new Date;

    const timestamp = (now.getFullYear().toString() + now.getMonth().toString() + now.getDate().toString() +
    now.getHours().toString() + now.getMinutes().toString() + now.getSeconds().toString());
    index++;
    createOrUpdateDbOneTable(dbName, tableName, id, idxs )
    add(dbName, tableName, {'details': `${document.getElementById('firstname').value}`, 'id': `${document.getElementById('location').value}_${timestamp}_${parseInt(index)}`, 
    'email': `${document.getElementById('email').value}`,
    'firstname': `${document.getElementById('firstname').value}`,
    'lastname': `${document.getElementById('lastname').value}`,
    'insurance': `${imageFile}`});
    
     /*  fetch(`https://localhost:3000/input`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        mode: 'cors',
        body: JSON.stringify({name: `${document.getElementById('firstname').value}`, id: `${document.getElementById('location').value}`, 'email': `${document.getElementById('email').value}`, 'insurance': `${imageFile}`}),
      }
      )
      .then((res) =>
      {
        res.text().then(value=>
          {
            if (value == 'success')
            {
              // readAll(dbName, tableName);
              deleteByKey(dbName, tableName, document.getElementById('location').value);
              readAll(dbName, tableName);
            }
          })
        
      })
      .catch(err =>
        {
         log('failed to get info');
        });*/
  });

}

function insertMultipleRecords()
{
  const id = 'id';
  const idxs = ['details'];
  getImageFile(function(imageFile)
  {
    const now = new Date;
    const timestamp = (now.getFullYear().toString() + now.getMonth().toString() + now.getDate().toString() +
    now.getHours().toString() + now.getMinutes().toString() + now.getSeconds().toString());
    createOrUpdateDbOneTable(dbName, tableName, id, idxs )
    const insertCount = document.getElementById('insertcount').value;
    for(let i=0; i < insertCount; i++)
    {
      index++;
      const data = {'details': `${document.getElementById('firstname').value}`, 'id': `${document.getElementById('location').value}_${timestamp}_${parseInt(index)}`, 
        'email': `${document.getElementById('email').value}`,
        'firstname': `${document.getElementById('firstname').value}`,
        'lastname': `${document.getElementById('lastname').value}`,
        'insurance': `${imageFile}`
      };
      add(dbName, tableName, data);
    }
  });
}


function deleteKey()
{
  log(document.getElementById('key').value);
  deleteByKey(dbName, tableName, document.getElementById('key').value);
}

function deleteMany()
{
  deleteMultipleKeys(dbName, tableName, parseInt(document.getElementById('count').value));
}

function log(msg)
{
    //alert(msg);
    console.log(msg);
    document.getElementById('out').innerHTML = `<pre>${msg}</pre>${document.getElementById('out').innerHTML}`;
}

/**
 * Initialize the app, gets the list of locations from local storage, then
 * renders the initial data.
 */
function init() {
  
  // Set up the event handlers for all of the buttons.
  // document.getElementById('btnFillSp1').addEventListener('click', fillSp1);
  // fetch
  document.getElementById('submitinfo').addEventListener('click', sendMessage);
  document.getElementById('deletekey').addEventListener('click', deleteKey);
  document.getElementById('deletemany').addEventListener('click', deleteMany);
  document.getElementById('insertMultiple').addEventListener('click', insertMultipleRecords);
}

init();
