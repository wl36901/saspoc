'use strict';



/*==============================================================================
   indexedDB
  
  ============================================================================*/

function createOrUpdateDbOneTable(dbName, tableName, id, idxs, dbversion)
{
    //param1 db name,
    // param2 db version
    // return: result (successful) is a instance or IDBOpenDBRequest obj(error)
    // status: success/error/upgradeneeded
    // evnet target is the request's obj
    let request;
    if (dbversion)
        request = window.indexedDB.open(dbName, dbversion);
    else
        request = window.indexedDB.open(dbName);

    
    // create a new db or upgrate a db (when the version chagne)
    request.onupgradeneeded = function(event)
    {
        const db = event.target.result; // get interface of database by event
        if (!db.objectStoreNames.contains(tableName))
        {
        // createObjectStore:
        // param 1: name
        // param 2: primary key 
            const objectStore = db.createObjectStore(tableName, { keyPath: id}) 
            {
                for (let i = 0; i < idxs.length; i ++)
                {
                    objectStore.createIndex(idxs[i], idxs[i], { unique: false}); // add index param: (indexname, indexfield, config)
                }
                
            }
        
        }   
        request.onerror = function(event) 
        {
           log(`Failed to open ${clientDB}`);
        }
        request.onsuccess = function(event)
        {
           log(`Created DB successfully`);
        }
    }
}


//============================================================
// after the init objecStore, we can add data to indexed db onsuccess (open db)
// data should be a objct
//====================================================
function add(dbName, tableName, data, version)
{
    const request = window.indexedDB.open(dbName, version)
    request.onsuccess = function(event)
    {
        const db = event.target.result;
        const requestAdd = db.transaction([tableName], 'readwrite')
        .objectStore(tableName)
        .add(data);
        
        requestAdd.onerror = function(event)
        {
           log(`Failed to write following data to the DB.  ${JSON.stringify(data)} `)
        }
        requestAdd.onsuccess = function(event)
        {
           log(`successfully written into the DB. ${JSON.stringify(data.id)} `);
        }
    }
} 

//==============================================================
// read by key
//===================================================
function readByKey(dbName, tableName, key, version)
{
    let request;
    if (version)
        request = window.indexedDB.open(dbName, version)
    else
        request = window.indexedDB.open(dbName)
    request.onsuccess = function(event)
    {
        const db = event.target.result;
        const requestRead = db.transaction([tableName])
        .objectStore(tableName)
        .get(key);
        
        requestRead.onerror = function(event)
        {
           log('Failed to read')
            // need to do
        }
        requestRead.onsuccess = function(event)
        {
          //  log(`successfully read`);
            
            return requestRead.result;
        }
    }
}

//==========================================================
// retrieve table
//====================================================
async function readAll(dbName, tableName, version)
{
    return new Promise(function(resolve, reject)
    {
    log('funciont readAll() ')
        let request;
        if (version)
            request = window.indexedDB.open(dbName, version)
        else
            request = window.indexedDB.open(dbName)
        const ret = [];
        request.onsuccess = function(event)
        {
            const db = event.target.result;
            const requestCursor = db.transaction([tableName])
            .objectStore(tableName)
            .openCursor();
            
            requestCursor.onerror = function(event)
            {
                log('Failed to read')
                reject('Failed to read');
            }
            requestCursor.onsuccess = function(event)
            {
            // log(`successfully read`);
                const cursor = event.target.result;
                if (cursor)
                {
                    //console.log(`cursor.key ${cursor.key}, cursor name ${cursor.value.name}, cursor email ${cursor.value.name}`);
                    ret.push(cursor.value);
                    cursor.continue();
                }
                else
                {
              //   log('no more row');
                    resolve(ret);
                }
            }
        }
    });
}

//=============================================================
//updat by key
//==========================================================
function updateByKey(dbName, tableName, key, newData, version)
{
    let request;
    if (version)
    {
        request = window.indexedDB.open(dbName, version);
    }
    else
    {
        request = window.indexedDB.open(dbName);
    }
    
    request.onsuccess = function(event)
    {
        const db = event.target.result;
      
        const requestUpdate = db.transaction([tableName]).objectStore(tableName).put(newData);
        requestUpdate.onerror = function(event)
        {

        };
        requestUpdate.onsuccess = function(event)
        {

        };

    };
    request.onerror = function(event)
    {

    }
}

//============================================================================
// delete data
//============================================================================
function deleteByKey(dbName, tableName, key, version)
{
    let request;
    if (version)
        request = window.indexedDB.open(dbName, version);
    else
        request = window.indexedDB.open(dbName);
    request.onsuccess = function(event)
    {
        const db = event.target.result;
        const requestDelete = db.transaction([tableName], 'readwrite').objectStore(tableName).delete(key);
        requestDelete.onsuccess = function()
        {
           log(`${key} deleted successfully`);
        }

    }
    request.onerror = function(event)
    {
        log(`Failed to delete ${key} with message ${event}`);
    }
   
}

//================= Delete Multiple keys ========//
async function deleteMultipleKeys(dbName, tableName, count, version) 
{
    const result = await readAll(dbName, tableName);
    if(result)
    {
        for(let i=0; i < result.length && i < count; i++)
        {
            const key = result[i].id;
            deleteByKey(dbName, tableName, key);
        }
    }

}
function log(msg)
{
    //alert(msg);
    console.log(msg);
    document.getElementById('out').innerHTML = `<pre>${msg}</pre>${document.getElementById('out').innerHTML}`;
}