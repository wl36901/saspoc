/*
 * @license
 * Your First PWA Codelab (https://g.co/codelabs/pwa)
 * Copyright 2019 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
'use strict';

// CODELAB: Update cache names any time any of the cached files change.
const CACHE_NAME = 'static-cache-v2';
const DATA_CACHE_NAME = 'data-cache-v2';

// CODELAB: Add list of files to cache here.
const FILES_TO_CACHE = [
 '/',
  '/index.html',
  '/scripts/app.js',
  '/scripts/install.js',
  // '/scripts/luxon-1.11.4.js',
  '/styles/inline.css',
  '/scripts/indexeddb.js',
  '/images/add.svg',
  '/images/clear-day.svg',
  '/images/clear-night.svg',
  '/images/cloudy.svg',
  '/images/fog.svg',
  '/images/hail.svg',
  '/images/install.svg',
  '/images/partly-cloudy-day.svg',
  '/images/partly-cloudy-night.svg',
  '/images/rain.svg',
  '/images/refresh.svg',
  '/images/sleet.svg',
  '/images/snow.svg',
  '/images/thunderstorm.svg',
  '/images/tornado.svg',
  '/images/wind.svg',
];

self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');
  // CODELAB: Precache static resources here.
  evt.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      console.log('[ServiceWorker] Pre-caching offline page');
      return cache.addAll(FILES_TO_CACHE);
    })
);

  self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  // CODELAB: Remove previous cached data from disk.
    evt.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (key !== CACHE_NAME && key !== DATA_CACHE_NAME ) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
);
  self.clients.claim();
});

self.addEventListener('fetch', (evt) => {
  console.log('server work fetch')
 evt.respondWith(
   caches.open(DATA_CACHE_NAME).then((cache) => 
   {
    //  console.log(`fetch event: ${evt.request}`);
    //  for(const i in evt.request)
    //  {
    //   console.log(`key is ${i}, value is ${evt.request[i]}`);
    //  }
    //  console.log(`evt.request end`);
    return cache.match(evt.request).then((response) => 
    {
      let fetchPromise = fetch(evt.request).then((netWorkResponse) =>
      {
        // console.log('~~~~~~response begin')
        // for(const i in netWorkResponse)
        // {
        //   console.log(`key is ${i}, value is ${netWorkResponse[i]}`);
        // }

        // console.log('~~~~~~~~~~~response end')
      
        // console.log(netWorkResponse.body.text());
        if (netWorkResponse.status === 200 && netWorkResponse.url !== 'https://localhost:3000/input' )
        {
          console.log('success fetch from internet~~~~~~~~~~');
          // 这个request 不能是post 请求，post请求的回退不能修改
          cache.put(evt.request, netWorkResponse.clone());
        }
        return netWorkResponse;
      })
      .catch(err =>
        {
          console.log(`fetch fail in fetch event`);
          return null;
        })
      return response || fetchPromise;
    })
   })
 )

});

self.addEventListener('sync', (evt) => {
  if (evt.tag === "send-messages") {
    evt.waitUntil(function() { 
        var sent = sendMessages(); 
        if (sent) {
            return Promise.resolve(); 
        }else{
            return Promise.reject(); 
            
        }
    }); 
}


});

function sendMessages()
    {
      
      // // ajax post
      // const xhr = new XMLHttpRequest();
      // xhr.open('POST', 'https:/localhost:3000/input', true);
      // xhr.send(null)

      // // call back function, it run at the last
      // xhr.onreadystatechange = function()
      // {
      //   let failedinfo = document.getElementById('failedinfo').value;
      //   const name = document.getElementById('name').value;
      //   if (this.readyState === 4 && this.status == 200) {
      //     const response = JSON.parse(this.responseText);
      //     console.log(`name: ${response.name}`);
      //     console.log(`status:  ${response.status}`);
      //     // get the response from server
      //     if(response.status !== 'success')
          
      //     failedinfo += `\n${name}`;
      //   }
      //   else
      //     failedinfo += `\n${name}`;
      // }
      // // send request
      // xhr.send(`name=${name}`);
      // return false;  // do not jump
    }
